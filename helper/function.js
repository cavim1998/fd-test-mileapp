export default {
  getDayLeft(dateTarget) {
    const dateNow = new Date()
    const target = new Date(dateTarget)
    const getMilisecondLeft = Math.abs(target - dateNow)
    return getMilisecondLeft / (24 * 60 * 60 * 1000)
  },
  formatPrice(value) {
    const val = (value / 1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  },
}
