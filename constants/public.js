export default {
  format: {
    //  untuk format nomor, tanggal, dll nanti kedepannya pakai yang ini aja
    date: 'dd-MMM-yyyy',
    time: 'hh:mm',
    dateTime: 'dd-MMM-yyyy HH:mm:ss',
    defaultDateTime: 'yyyy-MM-ddTHH:mm:ss',
    defaultDate: 'yyyy-MM-dd',
    defaultTime: 'HH:mm:ss',
    number: '#,##0.##',
    currency: '#,##0.#####',
    percent: '#0%',
    numberWithoutDecimal: '#0',
    accounting: '#,##0.#####;(#,##0.#####)',
  },
  regex: {
    hp: /^(^\+62\s?|^0)(\d{3,4} ?){2}\d{3,4}$/,
    number: /^[0-9]*$/,
    phone: /^([+]|[0-9])[0-9]*$/,
    // eslint-disable-next-line no-useless-escape
    phoneWithSpace: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
    // eslint-disable-next-line no-useless-escape
    fax: /^(\+?\d{1,}(\s?|\-?)\d*(\s?|\-?)\(?\d{2,}\)?(\s?|\-?)\d{3,}\s?\d{3,})$/,
    alphabetic: /^[A-Za-z]+$/,
    postalCode: /\d{5}/,
    // eslint-disable-next-line no-useless-escape
    email: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
    site: /^(https?:\/\/)?(www\.)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\.)+[\w]{2,}(\/\S*)?$/,
    decimal: /^\d*\.?\d*$/,
    // eslint-disable-next-line no-useless-escape
  },
  fundraising: {
    pending: 0,
    approved: 1,
    rejected: 2,
    pendingName: 'Pending',
    approvedName: 'Live',
    rejectedName: 'Rejected',
  },
  withdraw: {
    pending: 0,
    pendingName: 'Pending',
    approved: 1,
    approvedName: 'Setuju',
    rejected: 2,
    rejectedName: 'Ditolak',
    suspended: 3,
    suspendedName: 'Ditunda',
    canceled: 4,
    canceledName: 'Dibatalkan',
  },
  alertName: {
    pending: 0,
    approved: 1,
    rejected: 2,
  },
  paymentStatus: {
    pending: 1,
    success: 2,
    cancel: 3,
    expired: 4,
  },
}
