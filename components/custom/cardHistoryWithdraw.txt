<template>
  <div>
    <h4 v-if="listData.length === 0" class="text-center">Tidak Ada Data</h4>
    <b-card
      v-for="g of listData"
      v-else
      :key="g.Id"
      class="mb-2 mx-2"
      :class="{
        'border-primary': g.Status === $_const.withdraw.approved,
        'border-warning': g.Status === $_const.withdraw.pending,
        'border-danger':
          g.Status === $_const.withdraw.rejected ||
          g.Status === $_const.withdraw.suspended ||
          g.Status === $_const.withdraw.canceled,
      }"
      style="cursor: pointer"
      @click="showDetail(g)"
    >
      <div class="d-flex justify-content-between">
        <!--kiri-->
        <div>
          <p class="mb-0 font-weight-bolder">{{ g.BankName }}</p>
          <p class="mb-0 font-weight-bolder">{{ g.BankAccountNo }}</p>
          <p class="font-weight-bolder">{{ g.BankAccountName }}</p>
          <h5 class="font-weight-bolder text-primary">
            Rp{{ $_func.formatPrice(g.Amount) }}
          </h5>
        </div>

        <!--kanan-->
        <div class="ml-2" style="width: 300px">
          <p class="font-weight-bold border border-primary p-2 text-center">
            {{ g.CreatedOn }}
          </p>
          <p
            class="border text-center text-white py-2 px-3 mb-2 font-weight-bolder"
            :class="{
              'border-primary': g.Status === $_const.withdraw.approved,
              'bg-primary': g.Status === $_const.withdraw.approved,
              'border-warning': g.Status === $_const.withdraw.pending,
              'bg-warning': g.Status === $_const.withdraw.pending,
              'border-danger':
                g.Status === $_const.withdraw.rejected ||
                g.Status === $_const.withdraw.suspended ||
                g.Status === $_const.withdraw.canceled,
              'bg-danger':
                g.Status === $_const.withdraw.rejected ||
                g.Status === $_const.withdraw.suspended ||
                g.Status === $_const.withdraw.canceled,
            }"
          >
            {{ getStatusName(g) }}
          </p>
          <!-- <p><b>Keterangan:</b> {{ g.Description }}</p>
          <p><b>Alasan:</b> {{ g.Remarks }}</p> -->
        </div>
      </div>
    </b-card>
    <b-modal
      ref="sideDetail"
      title="Info Penarikan Dana"
      size="lg"
      centered
      hide-footer
    >
      <div v-if="selectedData" class="m-3">
        <p><b>Galang Dana:</b> {{ selectedData.Fundraising.Title }}</p>
        <p class="mb-0"><b>Nama Bank:</b> {{ selectedData.BankName }}</p>
        <p class="mb-0">
          <b>Rekening Bank:</b> {{ selectedData.BankAccountNo }}
        </p>
        <p><b>Atas Nama:</b> {{ selectedData.BankAccountName }}</p>
        <h5 class="font-weight-bolder text-primary">
          Rp{{ $_func.formatPrice(selectedData.Amount) }}
        </h5>
        <hr />
        <p><b>Alasan penarikan:</b> {{ selectedData.Description }}</p>
        <hr />
        <p class="text-center font-weight-bold">Riwayat Status</p>
        <ul class="progressbar">
          <li v-for="g of selectedData.WithdrawProgress" :key="g.Id">
            <div class="ml-2">
              <h5>{{ g.StatusName }}</h5>
              <p v-if="g.Status !== $_const.withdraw.pending" class="mb-0">
                {{ g.Remarks || '-' }}
              </p>
              <p>{{ g.CreatedOn }}</p>
            </div>
          </li>
        </ul>
      </div>
    </b-modal>
  </div>
</template>
<script>
export default {
  props: {
    listData: {
      type: Array,
      default: () => {},
    },
  },
  data() {
    return {
      selectedData: null,
      sideDetail: false,
    }
  },
  methods: {
    getStatusName(data) {
      const withConst = this.$_const.withdraw
      switch (data.Status) {
        case withConst.pending:
          return withConst.pendingName
        case withConst.approved:
          return withConst.approvedName
        case withConst.rejected:
          return withConst.rejectedName
        case withConst.suspended:
          return withConst.suspendedName
        case withConst.canceled:
          return withConst.canceledName
      }
    },
    showDetail(data) {
      // this.sideDetail = true
      this.$refs.sideDetail.show()
      this.selectedData = data
    },
  },
}
</script>
<style>
.progressbar {
  margin-left: 0.5rem;
  border-left: 3px solid #00b6f0;
}
.progressbar li {
  padding-left: 0.5rem;
  padding-bottom: 1.5rem;
  position: relative;
}
.progressbar li::before {
  content: '';
  width: 1rem;
  height: 1rem;
  background: #fff;
  border: 2px solid #00b6f0;
  border-radius: 50%;
  position: absolute;
  top: 0.3rem;
  left: -0.6rem;
}
</style>
