<template>
  <div class="wrap py-2">
    <b-tooltip
      v-if="hint"
      class="tooltipInfo"
      :target="`${label}-wrap`"
      placement="top"
      custom-class="tooltip"
    >
      <div>
        {{ hint }}
      </div>
    </b-tooltip>
    <label v-if="!right" :id="`${label}-wrap`" class="left-label" :for="label">
      {{ label }}
    </label>
    <b-form-checkbox
      :id="label"
      v-model="currentValue"
      switch
      :disabled="disabled"
      class="s-switch-box"
      @change="change"
    >
    </b-form-checkbox>
    <label v-if="right" :id="`${label}-wrap`" class="right-label" :for="label">
      {{ label }}
    </label>
  </div>
</template>

<script>
export default {
  components: {},

  props: {
    value: {
      type: Boolean,
      default: false,
    },

    label: {
      type: String,
      required: true,
    },
    right: {
      type: Boolean,
      default: true,
    },
    hint: {
      type: String,
      default: null,
    },
    disabled: {
      type: Boolean,
      default: false,
    },

    required: {
      type: Boolean,
      default: false,
    },

    placeholder: {
      type: String,
      default: '',
    },

    placeholderSuffix: {
      type: String,
      default: '',
    },
  },

  data: () => ({
    currentValue: null,
  }),

  computed: {},

  watch: {
    value: {
      immediate: true,
      handler(newVal) {
        this.currentValue = newVal
      },
    },

    currentValue: {
      handler(newVal) {
        this.$emit('input', newVal)
      },
    },
  },
  methods: {
    checkInput(e) {},
    change(e) {
      // this.$emit('input', e)
    },
  },
}
</script>
<style lang="scss" scoped>
.wrap {
  position: relative;
  display: flex;
  .left-label,
  .right-label {
    &:hover {
      cursor: pointer;
    }
  }
  .left-label {
    margin-right: 0.8em;
  }
  b-form-checkbox {
    &:hover {
      cursor: pointer;
    }
  }
}
</style>
