<template>
  <div>
    <ValidationProvider
      v-slot="{ errors }"
      :rules="selectedRules()"
      :name="label"
      :custom-messages="customMessages"
    >
      <b-form-group>
        <template v-if="showLabel" slot="label">
          <span>{{ label }}</span>
          <span v-if="required && !disabled" style="color: red">*</span>
        </template>
        <DxNumberBox
          v-model="currentValue"
          :disabled="disabled"
          :format="format || currentType()"
          :min="min"
          :max="max"
          :step="step"
          :read-only="readOnly"
          :show-clear-button="!required ? showClearButton : false"
          :visible="visible"
          :placeholder="placeholder || currentPlaceHolder"
        />
        <span class="text-danger small">{{ errors[0] }}</span>
        <template slot="description">
          <p v-if="errors.length === 0">{{ description }}</p>
        </template>
      </b-form-group>
    </ValidationProvider>
  </div>
</template>
<script>
import { DxNumberBox } from 'devextreme-vue/number-box'
import { ValidationProvider } from 'vee-validate'
export default {
  components: {
    DxNumberBox,
    ValidationProvider,
  },
  props: {
    value: {
      type: Number,
      default: null,
    },
    min: {
      type: Number,
      default: 0,
    },
    max: {
      type: Number,
      default: null,
    },
    step: {
      type: Number,
      default: 1,
    },
    required: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    readOnly: {
      type: Boolean,
      default: false,
    },
    showClearButton: {
      type: Boolean,
      default: true,
    },
    visible: {
      type: Boolean,
      default: true,
    },
    showLabel: {
      type: Boolean,
      default: true,
    },
    format: {
      type: String,
      default: null,
    },
    label: {
      type: String,
      default: '',
    },
    description: {
      type: String,
      default: null,
    },
    type: {
      type: String,
      default: null,
    },
    placeholder: {
      type: String,
      default: null,
    },
    ruleName: {
      type: String,
      default: 'default',
    },
    minValue: {
      type: String,
      default: null,
    },
  },
  data() {
    return {
      currentValue: null,
      currentPlaceHolder: `Masukkan ${this.label}`,
      customMessages: {
        donateValue: null,
      },
    }
  },
  watch: {
    value: {
      immediate: true,
      handler(newVal) {
        this.currentValue = newVal
      },
    },
    currentValue: {
      handler() {
        this.$emit('input', this.currentValue)
      },
    },
  },
  methods: {
    selectedRules() {
      switch (this.ruleName) {
        case 'default':
          return { required: this.required }
        case 'withMinValue':
          return { required: this.required, min_value: this.minValue }
        case 'nominal':
          this.customMessages.donateValue = `Tidak boleh kurang dari Rp${this.$_func.formatPrice(
            this.minValue
          )}`
          return { required: this.required, donateValue: this.minValue }
        default:
          return null
      }
    },
    currentType() {
      // ${this.defaultCurrency}
      if (this.type === 'currency') {
        return `${this.$_const.format.currency}`
      } else if (this.type === 'percent') {
        return `${this.$_const.format.numberWithoutDecimal}'%'`
      }
      return this.$_const.format.number
    },
  },
}
</script>
