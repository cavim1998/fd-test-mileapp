<template>
  <div>
    <b-sidebar
      v-model="showSide"
      title="Payment"
      right
      shadow
      no-close-on-esc
      no-close-on-backdrop
      backdrop
      width
    >
      <div class="px-3 py-2">
        <p>
          Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
          dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac
          consectetur ac, vestibulum at eros.
        </p>
        <b-img
          src="https://picsum.photos/500/500/?image=54"
          fluid
          thumbnail
        ></b-img>
      </div>
    </b-sidebar>
  </div>
</template>

<script>
export default {
  data() {
    return {
      showSide: false,
    }
  },
  methods: {
    show() {
      this.showSide = true
    },
    hide() {
      this.showSide = false
    },
  },
}
</script>
