export default {
  list(_context) {
    return this.$axios.get(`pokemon?limit=20000`)
  },
  getById(_context, id) {
    return this.$axios.get(`pokemon/${id}`)
  },
  create(_context, data) {
    return this.$axios.post(`/banner/save`, data)
  },
  update(_context, data) {
    return this.$axios.post(`/banner/update`, data)
  },
  delete(_context, data) {
    return this.$axios.post(`/banner/delete`, data)
  },
}
