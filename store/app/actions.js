export default {
  module(context, id) {
    return this.$_.find(
      context.state.role.RolePrivilege,
      (x) => x.ModuleId === id
    )
  },
  hasOtherModule(context, id) {
    return (
      this.$_.find(
        context.state.company.role.roleOtherModules,
        (x) => x.otherModuleId === id
      ) != null
    )
  },
  async hasModule(_context, id) {
    const module = await this.dispatch('app/module', id)
    return module != null
  },
  async canInsert(_context, id) {
    const module = await this.dispatch('app/module', id)
    return module ? module.AllowCreate : false
  },
  async canUpdate(_context, id) {
    const module = await this.dispatch('app/module', id)
    return module ? module.AllowUpdate : false
  },
  async canDelete(_context, id) {
    const module = await this.dispatch('app/module', id)
    return module ? module.AllowDelete : false
  },
  async canView(_context, id) {
    const module = await this.dispatch('app/module', id)
    return module ? module.AllowRead : false
  },
  async maxFileSize(_context) {
    try {
      const response = await this.$axios.get(`/api/company/maxFileSize`)
      return response.data
    } catch (err) {
      return null
    }
  }
}
