export default {
  setLoading(state) {
    state.loading = !state.loading
  },
}
