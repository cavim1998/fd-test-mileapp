import Vue from 'vue'
import customLoading from '@/components/custom/loading'

Vue.component('CustomLoading', customLoading)
