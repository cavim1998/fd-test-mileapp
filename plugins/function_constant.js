import functionHelp from '@/helper/function'

export default ({ app }, inject) => {
  inject('_func', functionHelp)
  app._ = functionHelp
}
