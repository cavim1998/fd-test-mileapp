import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA3OlPPMHKMqSocdlfhd7pNE7dSJxIJb1A',
    libraries: 'places',
  },
})
