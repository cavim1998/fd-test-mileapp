import publicConst from '@/constants/public'
// import settingConst from '@/constants/setting'

export default ({ app }, inject) => {
  inject('_const', publicConst)
  app._ = publicConst

  // inject('_setting', settingConst)
  // app._ = settingConst
}
